let day = require('moment')

console.time('now')
day()
console.timeEnd('now')

console.time('now + format')
day().format()
console.timeEnd('now + format')

console.time('now + format con string')
day().format("[YYYY] MM-DDTHH:mm:ssZ")
console.timeEnd('now + format con string')

console.time('now + new Date')
day(new Date(2018,8,18))
console.timeEnd('now + new Date')

console.time('now + clone')
day().clone()
console.timeEnd('now + clone')

console.time('now + month')
day().month()
console.timeEnd('now + month')

console.time('now + set')
day().set('month', 3)
console.timeEnd('now + set')

console.time('now + startOf + add + substract')
day().startOf('month').add(1, 'day').subtract(1, 'year')
console.timeEnd('now + startOf + add + substract')

console.time('now + tojson')
day().toJSON()
console.timeEnd('now + tojson')

console.time('now + toiso')
day().toISOString()
console.timeEnd('now + toiso')

console.time('now + isbefore')
day().isBefore(day())
console.timeEnd('now + isbefore')

console.time('now + issame')
day().isSame(day())
console.timeEnd('now + issame')

console.time('now + isleapyear')
day().isLeapYear()
console.timeEnd('now + isleapyear')

