const {performance} = require('perf_hooks')
let day = require('moment')
var t0, t1

t0 = performance.now()
day()
t1 = performance.now()
console.log("now: " + (t1 - t0) + " ms")

t0 = performance.now()
day().format()
t1 = performance.now()
console.log("now + format: " + (t1 - t0) + " ms")

t0 = performance.now()
day().format("[YYYY] MM-DDTHH:mm:ssZ")
t1 = performance.now()
console.log("now + format con string: " + (t1 - t0) + " ms")

t0 = performance.now()
day(new Date(2018,8,18))
t1 = performance.now()
console.log("now + new Date: " + (t1 - t0) + " ms")

t0 = performance.now()
day().clone()
t1 = performance.now()
console.log("now + clone: " + (t1 - t0) + " ms")

t0 = performance.now()
day().month()
t1 = performance.now()
console.log("now + month: " + (t1 - t0) + " ms")

t0 = performance.now()
day().set('month', 3)
t1 = performance.now()
console.log("now + set: " + (t1 - t0) + " ms")

t0 = performance.now()
day().startOf('month').add(1, 'day').subtract(1, 'year')
t1 = performance.now()
console.log("now + startOf + add + substract: " + (t1 - t0) + " ms")

t0 = performance.now()
day().toJSON()
t1 = performance.now()
console.log("now + tojson: " + (t1 - t0) + " ms")

t0 = performance.now()
day().toISOString()
t1 = performance.now()
console.log("now + toiso: " + (t1 - t0) + " ms")

t0 = performance.now()
day().isBefore(day())
t1 = performance.now()
console.log("now + isbefore: " + (t1 - t0) + " ms")

t0 = performance.now()
day().isSame(day())
t1 = performance.now()
console.log("now + issame: " + (t1 - t0) + " ms")

t0 = performance.now()
day().isLeapYear()
t1 = performance.now()
console.log("now + isleapyear: " + (t1 - t0) + " ms")

