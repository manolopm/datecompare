# Compare between [Day.js](https://github.com/xx45/dayjs) and [Moment.js](https://github.com/moment/moment)

I've tried to compare this two libraries, both have the same api.

* I launch each test 10 times and average the results. 
* I got the times with console.time and with performance.now methods. 
* I use node v8.9.4 to do the test.
* All the test were capture in the same computer in conditions as close as I could.
* The machine its an i7-3820 at 3.6Ghz with 32Gb of ram and debian 9.3
* The partial results are in the spreadsheet.
* All times are in ms.
* Positive differences day wins, negative ones moment wins.

## My final results are this:

* With console.time

|Function|Moment|Day|Difference|
|--------|:------:|:---:|:----------:|
|now|1,044|0,606|0,437|
|now + format|1,468|0,508|0,959|
|now + format con string|0,260|0,077|0,183|
|now + new Date|0,154|0,045|0,108|
|now + clone|0,095|0,329|-0,235|
|now + month|0,023|0,063|-0,040|
|now + set|0,411|0,360|0,051|
|now + startOf + add + substract|1,564|0,572|0,992|
|now + tojson|0,392|0,118|0,274|
|now + toiso|0,062|0,038|0,025|
|now + isbefore|0,276|0,083|0,193|
|now + issame|0,130|0,073|0,056|
|now + isleapyear|0,211|0,068|0,143|

* With performance.now

|Function|Moment|Day|Difference|
|--------|:------:|:---:|:----------:|
|now|1,405|0,546|0,859|
|now + format|1,773|0,482|1,291|
|now + format con string|0,346|0,066|0,280|
|now + new Date|0,092|0,042|0,050|
|now + clone|0,107|0,345|-0,238
|now + month|0,029|0,058|-0,029|
|now + set|0,384|0,360|0,024|
|now + startOf + add + substract|1,538|0,628|0,910|
|now + tojson|0,326|0,124|0,202|
|now + toiso|0,066|0,033|0,034|
|now + isbefore|0,122|0,074|0,049|
|now + issame|0,124|0,073|0,051|
|now + isleapyear|0,104|0,080|0,023|

